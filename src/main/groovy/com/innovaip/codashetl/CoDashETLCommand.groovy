package com.innovaip.codashetl

import com.innovaip.codashetl.services.FileScannerService
import com.innovaip.codashetl.services.ProcessSpeedTestService
import com.innovaip.codashetl.services.ProcessTutelaService
import io.micronaut.configuration.picocli.PicocliRunner
import picocli.CommandLine.Command
import picocli.CommandLine.Option

import javax.inject.Inject

@Command(name = "com/innovaip/codashetl", description = "...",
        mixinStandardHelpOptions = true)
class CoDashETLCommand implements Runnable {

    @Option(names = ["-s", "--sourceType"], required = true, description = "Source type: tutela, speedtest, repeaterMac")
    String sourceType

    @Inject
    FileScannerService fileScannerService

    static void main(String[] args) throws Exception {
        PicocliRunner.run(CoDashETLCommand.class, args)
    }

    @Inject ProcessTutelaService processTutelaService
    @Inject ProcessSpeedTestService processSpeedTestService

    void run() {
        switch (sourceType) {
            case null:
                println "help"
                break
            case "tutela":
                processTutelaService.start(sourceType)
                break
            case "speedtest":
                processSpeedTestService.start(sourceType)
                break
            case "reapeaterMac":
                println "Not implemented"
                break
            default:
                println "sourceType must be specified"
        }
    }
}
