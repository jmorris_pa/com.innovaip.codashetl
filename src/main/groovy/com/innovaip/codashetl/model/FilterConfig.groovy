package com.innovaip.codashetl.model

class FilterConfig {
    String[] expressions
    List<FilterConfigItem> filterItems

    FilterConfig(String[] expressions) {
        this.expressions = expressions
        filterItems = parseExpressions(this.expressions)
    }

    List<FilterConfigItem> parseExpressions(String[] expressions) {
        ArrayList<FilterConfigItem> items = new ArrayList<>()

        for (String expression in expressions) {
            String[] tokens = expression.split("\\s+")
            String fieldname = tokens[0]
            String operator = tokens[1]
            int posOperator = expression.indexOf(operator)
            String value = expression.substring(posOperator+1).trim()
            FilterConfigItem newItem = new FilterConfigItem(fieldname: fieldname, operator: tokens[1], value: value)
            items.add(newItem)
        }

        return  items
    }
}
