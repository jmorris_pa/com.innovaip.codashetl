package com.innovaip.codashetl.model

class ElasticBulkImportResponse {
    Long took
    Boolean errors
    Object items
}
