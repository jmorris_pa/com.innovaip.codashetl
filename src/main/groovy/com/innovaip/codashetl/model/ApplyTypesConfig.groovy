package com.innovaip.codashetl.model

import groovy.transform.InheritConstructors
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVRecord
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@InheritConstructors
class ApplyTypesConfig extends ArrayList<FieldType> {

    Logger log = LoggerFactory.getLogger(ApplyTypesConfig)


    ApplyTypesConfig(File format, String tag) {
        log.info("Loading type configuration from '${format.absolutePath}' with tag '${tag}'")
        String completeTag
        if (tag == "") {
            completeTag = ""
        } else {
            completeTag = tag + "_"
        }
        CSVParser csvFile = openAsCsv(format)

        for (CSVRecord record : csvFile) {
            String fieldname = completeTag + record.get(0)
            String type = record.get(1)
            log.info("Adding mapping for field '${fieldname}' with type '${type} ")
            add(new FieldType(fieldname: fieldname, type: type))
        }

    }

    CSVParser openAsCsv(File infile) {
        log.info("Openning file ${infile.absolutePath}")
        InputStreamReader input = new InputStreamReader(infile.newInputStream())
        CSVParser csvParser = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(input)
        csvParser
    }
}
