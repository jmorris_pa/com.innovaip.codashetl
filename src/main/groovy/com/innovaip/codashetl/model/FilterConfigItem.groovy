package com.innovaip.codashetl.model

class FilterConfigItem {
    String fieldname
    String operator
    String value
}
