package com.innovaip.codashetl.model

class FieldType {
    String fieldname
    /***
     * S = String
     * N = Long
     * F = Float
     */
    String type
}
