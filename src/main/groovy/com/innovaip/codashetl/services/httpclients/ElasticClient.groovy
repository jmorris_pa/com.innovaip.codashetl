package com.innovaip.codashetl.services.httpclients

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Header
import io.micronaut.http.annotation.Post
import io.micronaut.http.client.annotation.Client

@Client('${app.elasticurl}')
interface ElasticClient {


    @Post(uri = "/_bulk", consumes = MediaType.APPLICATION_OCTET_STREAM, produces =  MediaType.APPLICATION_OCTET_STREAM)
    @Header(value = "application/x-ndjson", name = "Content-Type")

    HttpResponse bulkPost(@Body String body, @Header(name = "Authorization") String authorization)
}
