package com.innovaip.codashetl.services

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.innovaip.codashetl.model.Errors
import com.innovaip.codashetl.model.RowPocessorList
import io.micronaut.context.ApplicationContext
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.slf4j.Logger
import org.slf4j.MDC

abstract class FileProcessorService {
    abstract String inputDir
    abstract String inputwildcard
    Logger log
    FileScannerService fileScannerService
    ApplicationContext applicationContext
    abstract void processInputFile(File infile)


    void start(String module) {
        MDC.put("module", module)
        String dir = getInputDir()
        String wildcard = getInputwildcard()
        log.info("Processing directory: ${dir} with wildcard ${wildcard}")
        //read input directory
        List<File> files =  fileScannerService.scanDirectory(dir, wildcard)
        log.info("Files found: ${files.size()} ")
        //process each file
        files.each { file ->
            log.info("Processing file: ${file.path}")
            processInputFile(file)
        }
    }


    Errors executeProcessors(RowPocessorList processList, Map<String, Object> hashRecord) {
        Errors errors = new Errors()
        processList.toSorted { item -> item.order }.each { rowPocessorListItem ->
            Errors processErrors
            def classtype = Class.forName(rowPocessorListItem.name)
            RowProcesor rowProcesor = (RowProcesor)  applicationContext.getBean(classtype)
            processErrors = rowProcesor.process(hashRecord, rowPocessorListItem.configuration)
            if (processErrors != null && processErrors.size() > 0 ) {
                processErrors*.source = rowPocessorListItem.name
                errors.addAll processErrors
            }
        }

        return errors
    }

    CSVParser openAsCsv(File infile, char separator = ',') {
        InputStreamReader input = new InputStreamReader(infile.newInputStream())
        CSVParser csvParser = CSVFormat.EXCEL.withFirstRecordAsHeader().withDelimiter(separator).parse(input)
        csvParser
    }

    void addControlRecords(Map<String, Object> hashRecord, String filename, long rowNumber, String insertDate, String tag = "tutela") {
        hashRecord.put("${tag}_file".toString(), filename)
        hashRecord.put("${tag}_fileRowNumber".toString(), rowNumber)
        hashRecord.put("${tag}_insertDate".toString(), insertDate)
    }

    String getDocumentId(Map<String, Object> hashRecord)
    {
        GsonBuilder gsonBuilder = new GsonBuilder()
        Gson gson = gsonBuilder.serializeNulls().create()
        String guid = DigestUtils.md5Hex(gson.toJson(hashRecord))
        return guid
    }
}
