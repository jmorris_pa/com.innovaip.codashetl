package com.innovaip.codashetl.services

import com.innovaip.codashetl.model.Errors

import java.util.HashMap
import java.util.List
import java.util.Map

interface RowProcesor {
    Errors process(Map<String, Object> input, Object configuration);
}
