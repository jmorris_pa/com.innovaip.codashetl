package com.innovaip.codashetl.services

import org.apache.commons.io.filefilter.WildcardFileFilter

import javax.inject.Singleton

@Singleton
class FileScannerService {

    List<File> scanDirectory(String directory, String wildcard) {
        File[] files = new File(directory).listFiles((FileFilter)new WildcardFileFilter(wildcard))

        if (files == null) {
            return new ArrayList<>()
        }

        List<File> listFiles = new ArrayList<>()
        Collections.addAll(listFiles, files)

        return listFiles
    }

}
