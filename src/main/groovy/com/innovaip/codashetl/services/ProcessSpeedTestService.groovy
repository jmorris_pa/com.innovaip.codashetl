package com.innovaip.codashetl.services

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.innovaip.codashetl.model.*
import com.innovaip.codashetl.services.httpclients.ElasticClient
import io.micronaut.context.ApplicationContext
import io.micronaut.context.annotation.Value
import io.micronaut.http.HttpResponse
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVRecord
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.annotation.PostConstruct
import javax.inject.Inject
import java.math.RoundingMode
import java.text.SimpleDateFormat

class ProcessSpeedTestService extends FileProcessorService {
    public static final String SPEEDTEST_USER_AGENT_FIELDNAME = "speedtest_userAgent"
    public static final String MAC_ADDRESS_FIELDNAME = "lease_relay_macAddress"
    @Value('${speedtest.inputdir}')
    String inputDir
    @Value('${speedtest.format}')
    String formatFile
    @Value('${speedtest.indexhash}')
    String elasticindex_datehash
    @Value('${speedtest.indexname}')
    String indexname
    @Value ('${speedtest.inputwildcard}')
    String inputwildcard

    @Value('${app.authentication}')
    String authentication

    Long elasticBatchUploadSize = 100

    ElasticClient elasticClient

    RowPocessorList rowPocessorList

    SearchSubscriberConfig searchSubscriberConfig
    ApplyTypesConfig applyTypesConfig
    UAParserConfig uaParserConfig

    @Inject
    ProcessTutelaService(ApplicationContext applicationContext,
                         ElasticClient elasticClient,
                         FileScannerService fileScannerService) {
        this.applicationContext = applicationContext
        this.elasticClient = elasticClient
        this.fileScannerService = fileScannerService
        this.log = LoggerFactory.getLogger(ProcessSpeedTestService.class)

        loadConfig()
    }

    @PostConstruct
    void loadConfig() {

        searchSubscriberConfig = new SearchSubscriberConfig(macAddressField: MAC_ADDRESS_FIELDNAME)

        applyTypesConfig = new ApplyTypesConfig(new File(formatFile), '')

        uaParserConfig = new UAParserConfig(fieldtag: "speedtest_", uafieldname: SPEEDTEST_USER_AGENT_FIELDNAME)

        rowPocessorList = new RowPocessorList()
        rowPocessorList.add(
                new RowProcessorListItem(name: "com.innovaip.codashetl.services.rowprocessor.SearchSubscriberRPService",
                        order: 0, configuration: searchSubscriberConfig)
        )
        rowPocessorList.add(
                new RowProcessorListItem(name: "com.innovaip.codashetl.services.rowprocessor.ApplyTypesRPService",
                        order: 1, configuration: applyTypesConfig)
        )

        rowPocessorList.add(
                new RowProcessorListItem(name: "com.innovaip.codashetl.services.rowprocessor.UAParserRowService",
                        order: 2, configuration: uaParserConfig)
        )

    }



    void processInputFile(File infile) {
        GsonBuilder gsonBuilder = new GsonBuilder()
        Gson gson = gsonBuilder.serializeNulls().create()
        List<String> jsonBuffer = new ArrayList<String>()
        Long counter = 0
        Date today = new Date()
        SimpleDateFormat dateHash = new SimpleDateFormat(elasticindex_datehash)
        String sDateHash = dateHash.format(today)
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        String insertDate = dateFormat.format(today)


        CSVParser csvParser = openAsCsv(infile, ';' as char)
        for (CSVRecord record : csvParser) {
            Long rowCounter = csvParser.currentLineNumber
            log.info("Processing Row ${rowCounter} from file ${infile.name}")
            Map<String, Object> mapRecord = (Map<String, Object>) record.toMap().collectEntries {k, v -> getField(k, v) }
            String documentId = getDocumentId(mapRecord)
            //custom fields
            //addCustomFields(mapRecord)

            //control records
            addControlRecords(mapRecord, infile.path, rowCounter, insertDate, "speedtest")
            Errors errors = executeProcessors(rowPocessorList, mapRecord)
            if (errors*.severity.contains(10)) {
                log.info("Ignoring row ${rowCounter} from file ${infile.name}")
                continue
            }
            calcKpi(mapRecord)
            String json = "{ \"index\" : { \"_index\" : \"${indexname}${sDateHash}\", \"_type\": \"doc\", \"_id\": \"${documentId}\" } }\n" + gson.toJson(mapRecord).toString()
            log.info("Importing Row ${rowCounter} from file ${infile.name}")
            jsonBuffer.add(json)
            if (counter == elasticBatchUploadSize) {
                String bulk = jsonBuffer.join("\n") + "\n"
                jsonBuffer.clear()
                def response = elasticClient.bulkPost(bulk, authentication)
                if (response.status.getCode() >= 300) {
                    log.warn("Error status returned from import: ${response.status.code} ")
                }

                Optional body = response.getBody(ElasticBulkImportResponse.class)
                ElasticBulkImportResponse parsedBody = body.get() as ElasticBulkImportResponse
                if (parsedBody && parsedBody.errors) {
                    log.warn("Error importing some lines")
                }
                counter = 0
            } else {
                counter++
            }
        }

        if (jsonBuffer.size() > 0)
        {
            String bulk = jsonBuffer.join("\n") + "\n"
            jsonBuffer.clear()
            HttpResponse response = elasticClient.bulkPost(bulk, authentication)
            if (response.status.getCode() >= 300) {
                log.warn("Error status returned from bulk import: ${response.status.code} ")
            }
        }

    }

    private void addCustomFields(Map<String, Object> mapRecord) {
        String sourceMacAddress = mapRecord.getOrDefault(MAC_ADDRESS_FIELDNAME, null)
        String formatedMacAddress = formatMacAddress(sourceMacAddress)
        mapRecord.put("lease_lease_macAddress_format", formatedMacAddress)
    }

    HashMap<String, Object> getField(String k, String v) {
        HashMap<String, Object> result = new HashMap<>()
        Object value = v == "null" ? null : v
        result.put(k.replace('.', '_'), value)
        result
    }

    /***
     * Format mac address from lease format (ej. 4070093e40c2) to Tytan format (ej. 40:70:09:3e:40:c2)
     * @param macAddress
     * @return
     */
    String formatMacAddress(String macAddress) {
        if (macAddress == null || macAddress.size() != 12) {
            return null
        }

        String formatedMacAddress = "${macAddress[0]}${macAddress[1]}:${macAddress[2]}${macAddress[3]}:${macAddress[4]}${macAddress[5]}:${macAddress[6]}${macAddress[7]}:${macAddress[8]}${macAddress[9]}:${macAddress[10]}${macAddress[11]}"

        return formatedMacAddress
    }

    void calcKpi(Map<String, Object> mapRecord) {
        BigDecimal downloadKpi
        BigDecimal uploadKpi
        BigDecimal combinedKpi

        Long downloadKbps = null
        Long planDownloadMbps = null
        try {
            downloadKbps = (Long) mapRecord.getOrDefault("speedtest_download_n", -1)
            planDownloadMbps = (Long) mapRecord.getOrDefault("tytan_bandwidth_down_n", -1)
        } catch (Exception exDownload) {
            //log.trace("Cannot convert download fields", exDownload)
        }

        Long uploadKpbs = null
        Long planUploadMbps = null
        try {
            uploadKpbs = (Long) mapRecord.getOrDefault("speedtest_upload_n", -1)
            planUploadMbps = (Long) mapRecord.getOrDefault("tytan_bandwidth_up_n", -1)
        } catch(Exception exUpload) {
            //log.trace("Cannot convert upload fields", exUpload)
        }

        downloadKpi = calculateRatio(downloadKbps, planDownloadMbps)
        uploadKpi = calculateRatio(uploadKpbs, planUploadMbps)

        if (downloadKpi != null && uploadKpi != null) {
            combinedKpi = Math.min(downloadKpi, uploadKpi).toBigDecimal()
        } else {
            combinedKpi = null
        }

        mapRecord.put("speedtest_download_kpi_n", downloadKpi)
        mapRecord.put("speedtest_upload_kpi_n", uploadKpi)
        mapRecord.put("speedtest_combine_kpi_n", combinedKpi)

        return
    }

    BigDecimal calculateRatio(Long testKbps, Long baseMbps) {
        BigDecimal result
        if (testKbps > 0 && baseMbps > 0) {
            result = 100 * (testKbps / (baseMbps * 1024))
        } else {
            result = null
        }

        return result?.setScale(2, RoundingMode.HALF_UP)
    }

    void setLogger(Logger logger) {
        this.log = logger
    }
}
