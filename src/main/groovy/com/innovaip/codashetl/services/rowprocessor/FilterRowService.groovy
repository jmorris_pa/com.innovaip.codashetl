package com.innovaip.codashetl.services.rowprocessor

import com.innovaip.codashetl.model.Error
import com.innovaip.codashetl.model.Errors
import com.innovaip.codashetl.model.FilterConfig
import com.innovaip.codashetl.services.RowProcesor
import groovy.transform.CompileStatic
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@javax.inject.Singleton
@CompileStatic
class FilterRowService implements RowProcesor {

    Logger log = LoggerFactory.getLogger(FilterRowService)

    @Override
    Errors process(Map<String, Object> input, Object configuration) {
        Errors errors = new Errors()
        FilterConfig config = (FilterConfig) configuration

        for (filter in config.filterItems) {
            String fieldname = filter.fieldname
            String operator = filter.operator
            String value = filter.value

            //evaluate operator contains
            if (operator == "contains") {
                if (!input.containsKey(fieldname)) {
                    Error error = new Error(message: "row filtered by expression ${fieldname} ${operator} ${value}")
                    errors.add(error)
                    continue
                }

                //stop evaluating because it is a contains operator
                continue
            }

            //evaluate boolean operators

            //check if fieldname exists in input
            if (!input.containsKey(fieldname)) {
                log.warn("input does not contains field ${fieldname}. Ignoring row")
                continue
            }

            switch (operator) {
                case "=":
                    String rowValue = input.get(fieldname)
                    if (rowValue == value) continue else {
                        Error error = new Error(message: "row filtered by expression ${fieldname} ${operator} ${value}", severity: 10)
                        errors.add(error)
                    }
                    break
                default:
                    log.warn("Unknown operator ${operator}")
            }
        }

        return errors
    }
}
