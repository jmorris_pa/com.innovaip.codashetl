package com.innovaip.codashetl.services.rowprocessor

import com.innovaip.codashetl.model.Error
import com.innovaip.codashetl.model.Errors
import com.innovaip.codashetl.model.SearchSubscriberConfig
import com.innovaip.codashetl.services.RowProcesor
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.transform.CompileStatic
import groovy.transform.Memoized
import io.micronaut.context.annotation.Value
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.annotation.PostConstruct

@CompileStatic
@javax.inject.Singleton
/***
 * Search subscriber on APS database
 */
class SearchSubscriberRPService implements RowProcesor  {

    Logger log = LoggerFactory.getLogger(SearchSubscriberRPService.class)
    Sql sql

    @Value('${ap.url}')
    String url
    @Value('${ap.user}')
    String user
    @Value('${ap.password}')
    String password
    @Value('${ap.driver}')
    String driver
    @Value('${ap.addmissingfields}')
    Boolean addMissingFields
    @Value('${ap.tytanfields}')
    String[] tytanFields


    @PostConstruct
    void openDbConnection() {
        sql = Sql.newInstance(url, user, password, driver)
    }

    Errors process(Map<String, Object> input, Object configuration) {
        SearchSubscriberConfig searchSubscriberConfig = (SearchSubscriberConfig) configuration
        Errors errors = new Errors()

        String macAddress = input.getOrDefault(searchSubscriberConfig.macAddressField, null)
        if (macAddress == "null" || macAddress == null) {
            def errorMessage = "Invalid mac address $macAddress"
            log.info(errorMessage)
            errors.add(new Error(message: errorMessage, severity: 2))
            addMissingFields(input)
        } else {
            macAddress = formatMacAddress(macAddress)
            log.info("Searching for macAddress: ${macAddress} on field ${searchSubscriberConfig.macAddressField}")
            List<GroovyRowResult> rows = getSubscriberData(macAddress)

            if (rows.size() == 1) {
                log.info("Found subscriber data for mac address ${macAddress}")
                def data = rows[0]
                data.each { k, v -> input.put("tytan_" + k, v) }
            } else {
                def errorMessage = "Subscriber not found data for mac address ${macAddress}"
                log.info(errorMessage)
                errors.add(new Error(message: errorMessage, severity: 2))
                addMissingFields(input)
            }
        }

        return errors
    }

    private void addMissingFields(Map<String, Object> input) {
        if (tytanFields && addMissingFields) {
            tytanFields.each {
                input.put(it, null)
            }
        }
    }

    @Memoized(maxCacheSize = 100000)
    private List<GroovyRowResult> getSubscriberData(String macAddress) {
        log.info("Searching for macAddress: ${macAddress} on database")
        List<GroovyRowResult> rows = sql.rows("SELECT subscriber.*, plan_group.bandwidth_down, plan_group.bandwidth_up, plan_group.name, plan_group.group_name, LEFT(plan_group.group_name, 3) as client_group  from subscriber left join plan_group on subscriber.plan_group_id = plan_group.id WHERE mac_address = :mac and subscriber.status = 1", [mac: macAddress])
        rows
    }

    String formatMacAddress(String mac) {
        if (mac == null) return null
        mac.replace(":", "")
    }
}
