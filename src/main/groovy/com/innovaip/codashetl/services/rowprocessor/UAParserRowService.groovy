package com.innovaip.codashetl.services.rowprocessor

import com.innovaip.codashetl.model.Errors
import com.innovaip.codashetl.model.UAParserConfig
import com.innovaip.codashetl.services.RowProcesor
import groovy.transform.CompileStatic
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import ua_parser.Client
import ua_parser.Parser

@javax.inject.Singleton
@CompileStatic
class UAParserRowService implements RowProcesor{
    Logger log = LoggerFactory.getLogger(UAParserRowService)
    Parser uaParser = new Parser()
    @Override
    Errors process(Map<String, Object> input, Object configuration) {
        UAParserConfig config = (UAParserConfig) configuration

        String ua = input.getOrDefault(config.uafieldname, null)

        if (ua == null || ua.empty) {
            return
        }

        //parse ua

        try {
            Client c = uaParser.parse(ua)

            input.put(config.fieldtag + "device", c.os.family)
            String osversion = "${c.os.major}${c.os.minor != null ? "." + c.os.minor : ""}"
            input.put(config.fieldtag + "osversion", osversion)
            input.put(config.fieldtag + "browser", c.userAgent.family )
            input.put(config.fieldtag + "browsermajorversion", c.userAgent.major )
            input.put(config.fieldtag + "browserminorversion", c.userAgent.minor )
        } catch (Exception exParser) {
            log.trace("Error parsing ua ${ua}. Ignoring", exParser)
        }
        return null
    }
}
