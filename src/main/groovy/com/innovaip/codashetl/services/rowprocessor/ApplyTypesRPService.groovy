package com.innovaip.codashetl.services.rowprocessor

import com.innovaip.codashetl.model.ApplyTypesConfig
import com.innovaip.codashetl.model.Errors
import com.innovaip.codashetl.model.FieldType
import com.innovaip.codashetl.services.RowProcesor
import groovy.transform.CompileStatic
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.text.SimpleDateFormat

@javax.inject.Singleton
@CompileStatic
class ApplyTypesRPService implements RowProcesor {

    Logger log = LoggerFactory.getLogger(ApplyTypesConfig)
    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")

    @Override
    Errors process(Map<String, Object> input, Object configuration) {
        ApplyTypesConfig config = (ApplyTypesConfig) configuration


        for( FieldType fieldType : config) {
            def fieldname = fieldType.fieldname
            Object value = filterNull(input.getOrDefault(fieldname, null))
            if (value == null)  continue
            switch (fieldType.type) {
                case "S":
                    break
                case "N":
                    String sValue = value
                    Long lValue = null
                    try {
                        lValue = parseLongDate(sValue)
                    } catch (Exception e) {
                        log.trace("Error converting field ${fieldname} to long. Value ${sValue}")
                    }
                    input.put(fieldname + "_n", lValue)
                    break
                case "F":
                    String sValue = value
                    Double fValue = null
                    try {
                        fValue = Double.parseDouble(sValue)
                    } catch (Exception e) {
                        log.trace("Error converting field ${fieldname} to double. Value ${sValue}")
                    }
                    input.put(fieldname + "_f", fValue)
                    break
                case "D":
                    String sValue = value
                    Long lValue = null
                    Date dValue = null
                    String sDateValue = null
                    try {
                        lValue = parseLongDate(sValue)
                        dValue = new Date(lValue * 1000)
                        sDateValue = dateFormat.format(dValue)
                    } catch (Exception e) {
                        log.trace("Error converting field ${fieldname} to float. Value ${sValue}")
                    }
                    input.put(fieldname + "_d", sDateValue)
                    break
                case "DS1":
                    String sValue = value
                    Date dValue = null
                    String sDateValue = null
                    try {
                        dValue = parseDS1(sValue)
                        sDateValue = dateFormat.format(dValue)
                    } catch (Exception e) {
                        log.trace("Error converting field ${fieldname} to float. Value ${sValue}")
                    }
                    input.put(fieldname + "_d", sDateValue)
                    break
                default:
                    log.error("Invalid field configuration ${fieldType.type} for field ${fieldname}")
            }
        }
    }

    Date parseDS1(String value) {
        //"Wed Jul 24 00:04:39 EST 2019"
        if (value == null) return null
        String[] tokens
        String date
        try {
            tokens = value.split(" ")
            date = "${tokens[2]}-${tokens[1]}-${tokens[5]} ${tokens[3]} ${tokens[4]}"
        } catch (Exception exParseDate) {
            log.info("Ignoring date: $value")
            return null
        }
        SimpleDateFormat ds1Format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss z")
        return ds1Format.parse(date)

    }

    Object filterNull(Object value) {
        if (value == "null") {return null} else {return value}
    }

    Long parseLongDate(String value) {
        String filteredValue = value
        if (value.endsWith(".0")) {
            filteredValue = value.substring(0, value.length()-2)
        }

        return Long.parseLong(filteredValue)
    }

}
