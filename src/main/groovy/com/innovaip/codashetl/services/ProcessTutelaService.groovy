package com.innovaip.codashetl.services

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.innovaip.codashetl.model.*
import com.innovaip.codashetl.services.httpclients.ElasticClient
import groovy.transform.CompileStatic
import io.micronaut.context.ApplicationContext
import io.micronaut.context.annotation.Value
import io.micronaut.http.HttpResponse
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVRecord
import org.slf4j.LoggerFactory

import javax.annotation.PostConstruct
import javax.inject.Inject
import javax.inject.Singleton
import java.text.SimpleDateFormat

@Singleton
@CompileStatic
class ProcessTutelaService extends FileProcessorService {
    public static final String TUTELA_CM_MAC_FIELDNAME = "tutela_CM_MAC"
    @Value('${tutela.indexhash}')
    String elasticindex_datehash
    @Value('${tutela.inputdir}')
    String inputDir
    @Value('${tutela.format}')
    String formatFile
    @Value('${tutela.indexname}')
    String indexname
    @Value ('${tutela.inputwildcard}')
    String inputwildcard
    @Value ('${tutela.filters}')
    String[] rowFilters
    @Value ('${tutela.cpemacfix}')
    Integer cpemacfix

    @Value('${app.authentication}')
    String authentication

    Long elasticBatchUploadSize = 100

    ElasticClient tutelaElasticClient

    RowPocessorList rowPocessorList

    SearchSubscriberConfig searchSubscriberConfig
    ApplyTypesConfig applyTypesConfig

    FilterConfig filters

    @Inject
    ProcessTutelaService(ApplicationContext applicationContext,
                         ElasticClient tutelaElasticClient,
                         FileScannerService fileScannerService) {
        this.applicationContext = applicationContext
        this.tutelaElasticClient = tutelaElasticClient
        this.fileScannerService = fileScannerService
        this.log = LoggerFactory.getLogger(ProcessTutelaService.class)
    }

    @PostConstruct
    void loadConfig() {

        searchSubscriberConfig = new SearchSubscriberConfig(macAddressField: TUTELA_CM_MAC_FIELDNAME)



        applyTypesConfig = new ApplyTypesConfig(new File(formatFile), 'tutela')
        rowPocessorList = new RowPocessorList()
        filters = new FilterConfig(rowFilters)
        rowPocessorList.add(
                new RowProcessorListItem(name: "com.innovaip.codashetl.services.rowprocessor.ApplyTypesRPService",
                        order: 0, configuration: applyTypesConfig)
        )
        rowPocessorList.add(
                new RowProcessorListItem(name: "com.innovaip.codashetl.services.rowprocessor.SearchSubscriberRPService",
                        order: 1, configuration: searchSubscriberConfig)
        )
        rowPocessorList.add(
                new RowProcessorListItem(name: "com.innovaip.codashetl.services.rowprocessor.FilterRowService",
                        order: 2, configuration: filters)
        )
    }


    void processInputFile(File infile) {
        GsonBuilder gsonBuilder = new GsonBuilder()
        Gson gson = gsonBuilder.serializeNulls().create()
        List<String> jsonBuffer = new ArrayList<String>()
        Long counter = 0
        Date today = new Date()
        SimpleDateFormat dateHash = new SimpleDateFormat(elasticindex_datehash)
        String sDateHash = dateHash.format(today)
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        String insertDate = dateFormat.format(today)



        CSVParser csvParser = openAsCsv(infile)
        for (CSVRecord record : csvParser) {
            Long rowCounter = csvParser.currentLineNumber
            log.info("Processing Row ${rowCounter} from file ${infile.name}")
            String sourceMacAddress = record.get("Connection_BSSID")
            if (sourceMacAddress == "null") continue
            Map<String, Object> mapRecord = (Map<String, Object>) record.toMap().collectEntries {k, v -> getField(k, v) }

            //custom fields
            addCustomFields(mapRecord)

            String formatedMAcAddress = formatMacAddres(sourceMacAddress)
            String cmMac
            try {
                cmMac = addToMacAddress(formatedMAcAddress, cpemacfix)
                mapRecord.put(TUTELA_CM_MAC_FIELDNAME, cmMac)
            } catch(Exception e) {
                log.warn("Error parsing mac address ${formatedMAcAddress}. Row ${rowCounter} from file ${infile.name} ", e)
                mapRecord.put(TUTELA_CM_MAC_FIELDNAME, formatedMAcAddress)
            }
            String documentId = getDocumentId(mapRecord)
            addControlRecords(mapRecord, infile.path, rowCounter, insertDate)
            Errors errors = executeProcessors(rowPocessorList, mapRecord)
            if (errors*.severity.contains(10)) {
                log.info("Ignoring row ${rowCounter} from file ${infile.name}")
                continue
            }
            String json = "{ \"index\" : { \"_index\" : \"${indexname}${sDateHash}\", \"_type\": \"doc\", \"_id\": \"${documentId}\" } }\n" + gson.toJson(mapRecord).toString()
            log.info("Importing Row ${rowCounter} from file ${infile.name}")
            jsonBuffer.add(json)
            if (counter == elasticBatchUploadSize) {
                String bulk = jsonBuffer.join("\n") + "\n"
                jsonBuffer.clear()
                HttpResponse response = tutelaElasticClient.bulkPost(bulk, authentication)
                if (response.status.getCode() >= 300) {
                    log.warn("Error status returned from import: ${response.status.code} ")
                }

                Optional body = response.getBody(ElasticBulkImportResponse.class)
                ElasticBulkImportResponse parsedBody = body.get() as ElasticBulkImportResponse
                if (parsedBody && parsedBody.errors) {
                    log.warn("Error importing some lines")
                }

                counter = 0
            } else {
                counter++
            }
        }

        if (jsonBuffer.size() > 0)
        {
            String bulk = jsonBuffer.join("\n") + "\n"
            jsonBuffer.clear()
            HttpResponse response = tutelaElasticClient.bulkPost(bulk, authentication)
            if (response.status.getCode() >= 300) {
                log.warn("Error status returned from bulk import: ${response.status.code} ")
            }
        }

    }

    String formatMacAddres(String value) {
        if (value == null) return null
        value.replaceAll(":", "")
    }

    HashMap<String, Object> getField(String k, String v) {
        HashMap<String, Object> result = new HashMap<>()
        Object value = v == "null" ? null : v
        result.put('tutela_' + k, value)
        result
    }

    String addToMacAddress(String macAddress, int add) {
        if (macAddress == null) return null
        BigInteger bi = new BigInteger(macAddress, 16)
        bi = bi + add
        String result = Long.toHexString(bi.longValue())
        result = result.padLeft(12, "0")
        return result
    }

    private void addCustomFields(Map<String, Object> mapRecord) {
        String wifiFrequency = mapRecord.getOrDefault("tutela_Connection_WifiFrequency", null)
        def range = getFrequencyRange(wifiFrequency)
        mapRecord.put("tutela_Connection_WifiFrequencyRange", range)
    }

    String getFrequencyRange(String frequency) {
        if (frequency == null) return null

        String result

        if (frequency.startsWith("5")) return "5"

        if (frequency.startsWith("2")) return "2.4"

        "UNKNOWN"
    }
}
