import com.internetitem.logback.elasticsearch.ElasticsearchAppender
import com.internetitem.logback.elasticsearch.config.ElasticsearchProperties
import com.internetitem.logback.elasticsearch.config.HttpRequestHeader
import com.internetitem.logback.elasticsearch.config.HttpRequestHeaders
import com.internetitem.logback.elasticsearch.config.Property

import java.nio.charset.Charset

// See http://logback.qos.ch/manual/groovy.html for details on configuration
appender('STDOUT', ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        charset = Charset.forName('UTF-8')

        pattern =
                '%d{yyyy-MM-dd HH:mm:ss.SSS} ' + // Date
                        '%5p ' + // Log level
                        '[%15.15t] ' + // Thread
                        '%-40.40logger{39} ' + // Logger
                        '%m%n' // Message
    }
}

appender("ELASTIC", ElasticsearchAppender){
    ElasticsearchProperties e = new ElasticsearchProperties()
    e.addProperty( new Property("logger", '%logger', false))
    e.addProperty( new Property("stacktrace", '%xEx', false))
    e.addProperty( new Property("level", '%p', false))
    e.addProperty( new Property("module", '%X{module}', true))
    component.properties = e

    url = 'https://instance:9200/_bulk'
    index = 'codashetl-logs-%date{yyyy-MM-dd}'
    type = 'log'
    rawJsonMessage = false
    errorsToStderr = true
    //authentication = new BasicAuthentication()
    def configHeaders = new HttpRequestHeaders()
    configHeaders.addHeader(new HttpRequestHeader(name: 'Content-Type', value: 'application/x-ndjson'))
    configHeaders.addHeader(new HttpRequestHeader(name: 'Authorization', value: 'Basic ZWxhc3RpYzpHdG1pdk1jVzhQRDY2SVBUd0NyMw=='))
    headers = configHeaders
}

//root(TRACE, ['STDOUT', 'ELASTIC'])
root(TRACE, ['STDOUT'])
