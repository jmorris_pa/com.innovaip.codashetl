package com.innovaip.codashetl.model

import spock.lang.Specification

class FilterConfigTest extends Specification {

    def "parse valid expression"() {
        FilterConfig filterConfig = new FilterConfig(validExpression)
        List<FilterConfigItem> items = filterConfig.filterItems
        expect:
        filterConfig != null
        items.size() > 0
        items[0].fieldname == "a"
        items[0].operator == "="
        items[0].value == "5"

        where:
        validExpression | _
        "a = 5"| _
        "a  =  5"| _
    }

}
