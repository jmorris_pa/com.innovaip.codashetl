package com.innovaip.codashetl.services


import spock.lang.Specification

class ProcessSpeedTestServiceTest extends Specification {
    ProcessSpeedTestService processSpeedTestService = new ProcessSpeedTestService()

    def "CalcKpi valid values"() {

        Map<String, Object> record = [speedtest_download_n: 5120,tytan_bandwidth_down_n: 100, speedtest_upload_n:2300, tytan_bandwidth_up_n: 5]
        processSpeedTestService.calcKpi(record)
        expect:
        record.get('speedtest_download_kpi_n') == 5.0
        record.get('speedtest_upload_kpi_n') == 44.92
        record.get('speedtest_combine_kpi_n') == 5.0
    }

    def "CalcKpi missing download values"() {

        Map<String, Object> record = [tytan_bandwidth_down_n: 100, speedtest_upload_n:2300, tytan_bandwidth_up_n: 5]
        processSpeedTestService.calcKpi(record)
        expect:
        record.get('speedtest_download_kpi_n') == null
        record.get('speedtest_upload_kpi_n') == 44.92
        record.get('speedtest_combine_kpi_n') == null
    }
    def "CalcKpi missing upload values"() {

        Map<String, Object> record = [speedtest_download_n: 5120, tytan_bandwidth_down_n: 100, speedtest_upload_n:2300, tytan_bandwidth_up_n: null]
        processSpeedTestService.calcKpi(record)
        expect:
        record.get('speedtest_download_kpi_n') == 5
        record.get('speedtest_upload_kpi_n') == null
        record.get('speedtest_combine_kpi_n') == null
    }

    def "CalcKpi strings values"() {

        Map<String, Object> record = [speedtest_download_n: 5120, tytan_bandwidth_down_n: 100, speedtest_upload_n:2300, tytan_bandwidth_up_n: "crazy value"]
        processSpeedTestService.calcKpi(record)
        expect:
        record.get('speedtest_download_kpi_n') == 5
        record.get('speedtest_upload_kpi_n') == null
        record.get('speedtest_combine_kpi_n') == null
    }

    def "CalculateRatio valid"(Long test, Long plan, BigDecimal expected) {

        BigDecimal result = processSpeedTestService.calculateRatio(test, plan )

        expect:
        result == expected

        where:
        test|plan|expected
        5120|5|100
        3000|5|58.59
        2300|100|2.25
        null|100|null
        2300|null|null
        null|null|null
    }
}
