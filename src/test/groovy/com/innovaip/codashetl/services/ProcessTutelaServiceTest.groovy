package com.innovaip.codashetl.services

import spock.lang.Specification

class ProcessTutelaServiceTest extends Specification {
    String validMacAddress = "00026f6ee348"
    String invalidMacAddress = "dfasdj"
    def "add valid mac address"() {
        ProcessTutelaService processTutelaService = new ProcessTutelaService(null, null, null)

        String result = processTutelaService.addToMacAddress(validMacAddress, 2)

        expect:

        result == "00026f6ee34a"
    }

    def "add invalid mac address"() {
        setup:
        ProcessTutelaService processTutelaService = new ProcessTutelaService(null, null, null)

        when:
        String result = processTutelaService.addToMacAddress(invalidMacAddress, 2)

        then:
        thrown NumberFormatException
    }

    def "add null mac address"() {
        ProcessTutelaService processTutelaService = new ProcessTutelaService(null, null, null)

        String result = processTutelaService.addToMacAddress(null, 2)

        expect:

        result == null
    }
}
