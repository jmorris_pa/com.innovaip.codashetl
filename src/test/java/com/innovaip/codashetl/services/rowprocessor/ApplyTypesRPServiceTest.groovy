package com.innovaip.codashetl.services.rowprocessor

import spock.lang.Specification

import java.text.SimpleDateFormat

class ApplyTypesRPServiceTest extends Specification {
    String validDate = "Wed Jul 24 00:04:39 EST 2019"
    String invalidDate1 = "Wed Jul 24 00:04:39 EST"
    def "Parse Valid Date"() {
        given:
        SimpleDateFormat fmtDay = new SimpleDateFormat("dd")
        SimpleDateFormat fmtYear = new SimpleDateFormat("yyyy")
        ApplyTypesRPService applyTypesRPService = new ApplyTypesRPService()
        Date parsedDate = applyTypesRPService.parseDS1(validDate)

        expect:
        applyTypesRPService != null
        fmtDay.format(parsedDate) == "24"
        fmtYear.format(parsedDate) == "2019"
    }

    def "Parse invalidDates"() {
        given:
        ApplyTypesRPService applyTypesRPService = new ApplyTypesRPService()
        Date parsedDate = applyTypesRPService.parseDS1(invalidDate1)

        expect:
        parsedDate == null
    }
}
